package rencryption

type Encryptor interface {
	Encrypt(plain []byte) (ciphertext []byte, err error)
	EncryptString(ciphertext []byte) (res string)
}

type Decryptor interface {
	Decrypt(ciphertext []byte) (plainByte []byte, err error)
	DecryptString(ciphertext []byte) (res string)
}

type Encryption interface {
	Encryptor
	Decryptor
}
