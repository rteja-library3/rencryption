package rencryption

import (
	"encoding/base32"
)

type base32Encryption struct {
	hexKey []byte
}

func NewBase32Encryption(key string) Encryption {
	return &base32Encryption{
		hexKey: []byte(key),
	}
}

func (b base32Encryption) Encrypt(plain []byte) (ciphertext []byte, err error) {
	ciphertext = make([]byte, base32.StdEncoding.EncodedLen(len(plain)))
	base32.StdEncoding.Encode(ciphertext, plain)
	return
}

func (b base32Encryption) Decrypt(ciphertext []byte) (plainByte []byte, err error) {
	// var n int
	// plainByte = make([]byte, base32.StdEncoding.DecodedLen(len(ciphertext)))
	// n, err = base32.StdEncoding.Decode(plainByte, ciphertext)
	// plainByte = plainByte[:n]

	plainByte, err = base32.StdEncoding.DecodeString(string(ciphertext))
	return
}

func (b base32Encryption) EncryptString(ciphertext []byte) (res string) {
	return string(ciphertext)
}

func (b base32Encryption) DecryptString(ciphertext []byte) (res string) {
	return string(ciphertext)
}
