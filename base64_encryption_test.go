package rencryption_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rencryption"
)

func TestBase64EncryptSuccess(t *testing.T) {
	enc := rencryption.NewBase64Encryption("test-key")

	chiper, err := enc.Encrypt([]byte("password-tes"))

	assert.NoError(t, err, "[TestBase64EncryptSuccess] Should not error")
	assert.Less(t, 0, len(chiper), "[TestBase64EncryptSuccess] byte should not empty")
	assert.Equal(
		t,
		[]byte{99, 71, 70, 122, 99, 51, 100, 118, 99, 109, 81, 116, 100, 71, 86, 122},
		chiper,
		"[TestBase64EncryptSuccess] Chipper should same",
	)
}

func TestBase64DecryptSuccess(t *testing.T) {
	enc := rencryption.NewBase64Encryption("test-key")

	plain, err := enc.Decrypt([]byte("cGFzc3dvcmQtdGVz"))

	assert.NoError(t, err, "[TestBase64DecryptSuccess] Should not error")
	assert.Less(t, 0, len(plain), "[TestBase64DecryptSuccess] byte should not empty")
	assert.Equal(
		t,
		[]byte{112, 97, 115, 115, 119, 111, 114, 100, 45, 116, 101, 115},
		plain,
		"[TestBase64DecryptSuccess] Plain should same",
	)
}

func TestBase64DecryptFailed(t *testing.T) {
	enc := rencryption.NewBase64Encryption("test-key")

	_, err := enc.Decrypt([]byte("XXXXXaGVsbG8=")) // cGFzc3dvcmQtdGVz

	assert.Error(t, err, "[TestBase64DecryptFailed] Should error")
}

func TestBase64DecryptStringSuccess(t *testing.T) {
	var enc rencryption.Decryptor = rencryption.NewBase64Encryption("test-key")

	plain, err := enc.Decrypt([]byte("cGFzc3dvcmQtdGVz"))

	s := enc.DecryptString(plain)

	assert.NoError(t, err, "[TestBase64StringSuccess] Should not error")
	assert.Equal(t, "password-tes", s, "[TestBase64StringSuccess] String should \"password-tes\"")
}

func TestBase64EncryptStringSuccess(t *testing.T) {
	var enc rencryption.Encryptor = rencryption.NewBase64Encryption("test-key")

	plain, err := enc.Encrypt([]byte("password-tes"))

	s := enc.EncryptString(plain)

	assert.NoError(t, err, "[TestBase64EncryptStringSuccess] Should not error")
	assert.Equal(t, "cGFzc3dvcmQtdGVz", s, "[TestBase64EncryptStringSuccess] String should \"OBQXG43XN5ZGILLUMVZQ====\"")
}
